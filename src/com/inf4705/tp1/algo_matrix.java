package com.inf4705.tp1;

/**
 * Created by abmala on 18-09-14.
 */
public interface algo_matrix {
    long getTime();
    int[][] get_resulting_matrix();
    void show_matrix(int[][] M);
}
