package com.inf4705.tp1.Utils;
import java.io.File;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
/**
 * Created by abmala on 18-09-14.
 */

public class Reading_Matrix
{
    static String folder = System.getProperty("user.dir") + "/src/ressources/matrix/";

    public int[][] read_func(int N_, int I) throws IOException{
        int[][] array = null;
        String fileName = "ex_" + N_ + "." + I;
        File file = new File(Reading_Matrix.folder + fileName);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line;
        if((line = bufferedReader.readLine()) != null) {
            int N = Integer.parseInt(line);
            array = new int[(int)Math.pow(2, N)][(int)Math.pow(2, N)];
            int index = 0;
            while ((line = bufferedReader.readLine()) != null) {
                String[] sT = line.split("\t");
                for (int i=0; i< sT.length; ++i) {
                    array[index][i] = Integer.parseInt(sT[i]);
                }
                ++index;
            }
        }
        return array;
    }

    public int[][] read_func(String fileName) throws IOException{
        int[][] array = null;
        File file = new File(fileName);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line;
        if((line = bufferedReader.readLine()) != null) {
            int N = Integer.parseInt(line);
            array = new int[(int)Math.pow(2, N)][(int)Math.pow(2, N)];
            int index = 0;
            while ((line = bufferedReader.readLine()) != null) {
                String[] sT = line.split("\t");
                for (int i=0; i< sT.length; ++i) {
                    array[index][i] = Integer.parseInt(sT[i]);
                }
                ++index;
            }
        }
        return array;
    }

}