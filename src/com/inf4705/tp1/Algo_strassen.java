package com.inf4705.tp1;

import com.inf4705.tp1.Utils.Reading_Matrix;
import org.omg.CORBA.MARSHAL;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.*;
import java.lang.Math;

/**
 * Created by soelga on 18-09-14.
 */
class Algo_strassen implements algo_matrix {

    static int[][] result;
    double time_calculation = 0;
    @Override
    public long getTime() {
        return (long)this.time_calculation;
    }

    public double getTime_() {
        return this.time_calculation/(double)Math.pow(10,6);
    }

    @Override
    public int[][] get_resulting_matrix() {
        return this.result;
    }


    public int[][] multiply(int[][] Matrix_A, int[][] Matrix_B) {

        if(Matrix_A.length < 2 && Matrix_B.length < 2){
            int[][] C = new int[1][1];
            C[0][0] = Matrix_A[0][0] * Matrix_B[0][0];
            return C;
        }else{
            int[][] A11 =null, A12=null, A21=null, A22=null;
            A11 = split_matrix(Matrix_A, 0, Matrix_A.length/2, 0, Matrix_A.length/2);

            A21 = split_matrix(Matrix_A, Matrix_A.length/2, Matrix_A.length, 0, Matrix_A.length/2);
            A12 = split_matrix(Matrix_A, 0, Matrix_A.length/2, Matrix_A.length/2, Matrix_A.length);

            A22 = split_matrix(Matrix_A, Matrix_A.length/2, Matrix_A.length, Matrix_A.length/2, Matrix_A.length);

            int[][] B11=null, B12=null, B21=null, B22=null;
            B11 = split_matrix(Matrix_B, 0, Matrix_B.length/2,0, Matrix_B.length/2);
            B21 = split_matrix(Matrix_B, Matrix_B.length/2, Matrix_B.length,0, Matrix_B.length/2);
            B12 = split_matrix(Matrix_B, 0, Matrix_B.length/2, Matrix_B.length/2, Matrix_B.length);
            B22 = split_matrix(Matrix_B,Matrix_B.length/2, Matrix_B.length,Matrix_B.length/2, Matrix_B.length);

            int[][] M1=null, M2=null, M3=null, M4=null, M5=null, M6=null, M7=null;
            M1 = multiply(addition(A11, A22), addition(B11, B22));
            M2 = multiply(addition(A21, A22), B11);
            M3 = multiply(A11, substraction(B12, B22));
            M4 = multiply(A22, substraction(B21, B11));
            M5 = multiply(addition(A11, A12), B22);
            M6 = multiply(substraction(A21, A11), addition(B11, B12));
            M7 = multiply(substraction(A12, A22), addition(B21, B22)); 

            int[][] C11= null, C12=null, C21=null, C22=null;

            C11 = addition(substraction(addition(M1,M4),M5),M7);
            C12 = addition(M3,M5);
            C21 = addition(M2,M4);
            C22 = addition(substraction(addition(M1,M3),M2),M6);
            int[][] Matrix_C = new int[Matrix_A.length][Matrix_A.length];
            put_it_all_together(C11, C12, C21, C22, Matrix_C);
            
            return Matrix_C;
        }
    }

    private int[][] split_matrix(int[][] matrix, int index_min_i, int index_max_i, int index_min_j, int index_max_j){
        int[][] Sub_matrix = new int[index_max_i-index_min_i][index_max_j-index_min_j];
        int sub_i = 0;

        for(int i=index_min_i; i<index_max_i; ++i) {
            int sub_j = 0;
            for (int j = index_min_j; j < index_max_j; ++j) {
                Sub_matrix[sub_i][sub_j] = matrix[i][j];
                ++sub_j;
            }
            ++sub_i;
        }
        return Sub_matrix;

    }

    private int[][] addition(int[][] matrix_1, int[][] matrix_2){
        int m_size = matrix_1.length;
        int [][] the_matrix = new int[m_size][m_size];
        for(int i=0; i< m_size ;++i){
            for(int j=0; j<m_size; ++j){
                the_matrix[i][j] = matrix_1[i][j] + matrix_2[i][j];
            }
        }
        return the_matrix;
    }

    private int[][] substraction(int[][] matrix_1, int[][] matrix_2){
        int m_size = matrix_1.length;
        int [][] the_matrix = new int[m_size][m_size];
        for(int i=0; i< m_size ;++i){
            for(int j=0; j<m_size; ++j){
                the_matrix[i][j] = matrix_1[i][j] - matrix_2[i][j];
            }
        }
        return the_matrix;
    }

    private void put_it_all_together(int[][] C1, int[][] C2, int[][] C3, int[][] C4, int[][] C){
        int half_m_size = C.length/2;
        for(int i=0; i< half_m_size ; ++i){
            for(int j=0; j< half_m_size ; ++j){
                C[i][j] = C1[i][j];
                C[i][j + half_m_size] = C2[i][j];
                C[i + half_m_size][j] = C3[i][j];
                C[i + half_m_size][j + half_m_size] = C4[i][j];
            }
        }
    }

    public  void strassen(int[][] Matrix_A, int[][] Matrix_B) {
        if(
                Matrix_A.length > 0 &&
                        Matrix_B.length > 0 &&
                        Matrix_A.length == Matrix_A[0].length &&
                        Matrix_B.length == Matrix_B[0].length &&
                        Matrix_A.length == Matrix_B.length
                )
        {
            result = new int[Matrix_A.length][Matrix_A.length];
            int size_matrix = Matrix_A.length;

            int[][] C = null;
            C = this.multiply(Matrix_A, Matrix_B);
            result = C;
        }else{
            System.err.println("[ERROR] The matrix don't have the same size.");
        }
    }

    @Override
    public void show_matrix(int[][] M){
        String matrix_print = "\n";
        for(int i=0; i<M.length ; ++i){
            for(int j=0; j< M[i].length ;++j){
                matrix_print += (M[i][j] + "\t");
            }
            matrix_print += "\n";
        }
        System.out.println(matrix_print);
    }

    private void reloadFileWithNewContent(String time, int N){
        String Folder_path = "src/ressources/resultats/";
        String fileName = "strassen_" + N;
        String toWrite = time + ";";
        try {
            FileWriter fw = new FileWriter(Folder_path+fileName+".csv", true);
            BufferedWriter bw = new BufferedWriter(fw);
            fw.write(toWrite);
            bw.close();
            fw.close();
        }catch(IOException e){
            System.err.println("error");
        }
    }

    public static void main(String[] args)
    {
        if(args == null || args.length == 0)
        {
            args = new String[4];
            args[0] = "2";
            args[1] = "1";
            args[2] = "2";
            System.out.println("[WARNING] pas d'argument");
        }
        int N = 0;
        int I1 = 0;
        int I2 = 0;

        if(args != null && args.length >= 3) {
            N = Integer.parseInt(args[0]);
            I1 = Integer.parseInt(args[1]);
            I2 = Integer.parseInt(args[2]);
            String a = args[3];

            if(a.equals("null") || !a.equals("strassen")){
                return;
            }
            int p = Integer.parseInt(args[4]);
            int t = Integer.parseInt(args[5]);
            String path1 = args[6];
            String path2 = args[7];

            Reading_Matrix reader = new Reading_Matrix();
            boolean reading_success = false;
            int[][] Matrix_A = null;
            int[][] Matrix_B = null;
            try {
                if( path1.equals("-1") && path2.equals("-1")) {
                    Matrix_A = reader.read_func(N, I1);

                    Matrix_B = reader.read_func(N, I2);
                }else{
                    Matrix_A = reader.read_func(path1);
                    Matrix_B = reader.read_func(path2);
                }

                reading_success = true;
            } catch (IOException IO) {
                System.out.println("ERROR : " + IO.getLocalizedMessage());
                reading_success = false;
            }
            if (reading_success) {
                Algo_strassen algo = new Algo_strassen();
                double start_time = (double)System.nanoTime();
                algo.strassen(Matrix_A, Matrix_B);
                double stop_time = (double)System.nanoTime();
                double time_total = (stop_time - start_time)/(double)Math.pow(10,6);
                if(t > 0){
                    System.out.println(time_total);
                }
                if(p > 0){
                    algo.show_matrix(result);
                }

                String time_str = time_total + "";
                 if(I1 == 4 && I2 == 5){
                     time_str += "\n";
                }
                algo.reloadFileWithNewContent(time_str+"", N);
                
            }
        }else{
            System.err.println("[WARNING] Les arguments envoyer a Algo_strassen.java ne sont pas correct!");
        }
    }
}
