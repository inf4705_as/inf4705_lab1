/**
 * Created by abmala and Soukaina on 18-09-14.
 */

package com.inf4705.tp1;
import java.io.IOException;
import com.inf4705.tp1.Utils.Reading_Matrix;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.*;
import java.lang.Math;

class Algo_conventionnel implements algo_matrix {
    static int[][] result;
    double time_calculation = 0;
    @Override
    public long getTime() {
        return (long)this.time_calculation;
    }

    public double getTime_() {
        return this.time_calculation/(double)Math.pow(10,6);
    }

    @Override
    public int[][] get_resulting_matrix() {
        return this.result;
    }

    public  void multiply(int[][] Matrix_A, int[][] Matrix_B) {
        if(
                Matrix_A.length > 0 &&
                        Matrix_B.length > 0 &&
                        Matrix_A.length == Matrix_A[0].length &&
                        Matrix_B.length == Matrix_B[0].length &&
                        Matrix_A.length == Matrix_B.length
                )
        {
            result = new int[Matrix_A.length][Matrix_A.length];
            int size_matrix = Matrix_A.length;
            for(int i=0; i<size_matrix ; ++i){
                for(int j=0; j<size_matrix ; ++j){
                    for(int k=0; k<size_matrix ; ++k)
                    result[i][j] += Matrix_A[i][k] * Matrix_B[k][j];
                }
            }
        }else{
            System.out.println("[ERROR] The matrix don't have the same size.");
        }
    }

    @Override
    public void show_matrix(int[][] M){
        String matrix_print = "\n";
        for(int i=0; i<M.length ; ++i){
            for(int j=0; j< M[i].length ;++j){
                matrix_print += (M[i][j] + "\t");
            }
            matrix_print += "\n";
        }
        System.out.println(matrix_print);
    }

    private void reloadFileWithNewContent(String time, int N){
        String Folder_path = "src/ressources/resultats/";
        String fileName = "conv_" + N;
        String toWrite = time + ";";
        try {
            FileWriter fw = new FileWriter(Folder_path+fileName+".csv", true);
            BufferedWriter bw = new BufferedWriter(fw);
            fw.write(toWrite);
            bw.close();
            fw.close();
        }catch(IOException e){
            System.err.println("error");
        }
    }


    public static void main(String[] args)
    {
        if(args == null || args.length == 0)
        {
            args = new String[4];
            args[0] = "2";
            args[1] = "1";
            args[2] = "2";
            System.out.println("[WARNING] pas d'argument");
        }
        int N = 0;
        int I1 = 0;
        int I2 = 0;

        if(args != null && args.length >= 3) {
            N = Integer.parseInt(args[0]);
            I1 = Integer.parseInt(args[1]);
            I2 = Integer.parseInt(args[2]);
            String a = args[3];

            if(a.equals("null") || !a.equals("conv")){
                return;
            }
            int p = Integer.parseInt(args[4]);
            int t = Integer.parseInt(args[5]);
            String path1 = args[6];
            String path2 = args[7];

            Reading_Matrix reader = new Reading_Matrix();
            boolean reading_success = false;
            int[][] Matrix_A = null;
            int[][] Matrix_B = null;
            try {
                if( path1.equals("-1") && path2.equals("-1")) {
                    Matrix_A = reader.read_func(N, I1);
                    Matrix_B = reader.read_func(N, I2);
                }else{
                    Matrix_A = reader.read_func(path1);
                    Matrix_B = reader.read_func(path2);
                }
                reading_success = true;
            } catch (IOException IO) {
                System.out.println("ERROR : " + IO.getLocalizedMessage());
                reading_success = false;
            }
            if (reading_success) {
                Algo_conventionnel algo = new Algo_conventionnel();
                double start_time = (double)System.nanoTime();
                algo.multiply(Matrix_A, Matrix_B);
                double stop_time = (double)System.nanoTime();
                double time_total = (stop_time - start_time)/(double)Math.pow(10,6);
                if(t > 0){
                    System.out.println(time_total);
                }
                if(p > 0){
                    algo.show_matrix(result);
                }
                String time_str = time_total + "";
                 if(I1 == 4 && I2 == 5){
                     time_str += "\n";
                }
                algo.reloadFileWithNewContent(time_str+"", N);
                
            }
        }else{
            System.err.println("[WARNING] Les arguments envoyer a Algo_conventionnel.java ne sont pas correct!");
        }
    }
}
