import subprocess, sys, os

max_matrix_size = 11

STRASSEN = "strassen"
CONV = "conv"
STRASSEN_SEUIL = "strassenSeuil"
java_command_conv     = ["java", "-Xmx1g", "-Xms1g", "-jar", "build/jar/Algo_conventionnel.jar"]
java_command_strassen = ["java", "-Xmx1g", "-Xms1g", "-jar", "build/jar/Algo_strassen.jar"]
java_command_strassen_seuil = ["java", "-Xmx1g", "-Xms1g", "-jar", "build/jar/Algo_strassen_seuil.jar"]


if len(sys.argv) > 1 and '-a' in sys.argv or ('-e' in sys.argv or '-p' in sys.argv or '-t' in sys.argv):
    index_arg_a = -1
    index_arg_e = -1
    index_arg_p = -1
    index_arg_t = -1
    index_path1 = -1
    index_path2 = -1
    index_conv = -1
    index_strassen = -1
    index_strassen_seuil = -1

    if '-a' in sys.argv:
        index_arg_a = sys.argv.index('-a')
        pass
    if '-e' in sys.argv:
        index_arg_e = sys.argv.index('-e')
        index_path1 = index_arg_e + 1
        index_path2 = index_path1 + 1
        pass
    if '-p' in sys.argv:
        index_arg_p = sys.argv.index('-p')
        pass
    if '-t' in sys.argv:
        index_arg_t = sys.argv.index('-t')
        pass
    if CONV in sys.argv:
        index_conv = sys.argv.index(CONV)
        pass
    if STRASSEN in sys.argv:
        index_strassen = sys.argv.index(STRASSEN)
        pass
    if STRASSEN_SEUIL in sys.argv:
        index_strassen_seuil = sys.argv.index(STRASSEN_SEUIL)
        pass

    # Creation des jar

    os.environ["ANT_OPTS"] = "-Xmx2048m"
    subprocess.call(["ant", "-q", "-S","jar"])

    # Logique pour la multiplication de toutes les combinaisons de matrices entre elles.
    # Boucle pour les N de 1 jusqu'a la taille max requise.
    if index_arg_e <= 0 :
        for N in range(1, max_matrix_size):
            # 1ere boucle de 1 a 4
            for I1 in range(1,5):
                # 2eme boucle de I1 jusqu'a 5
                for I2 in range((I1 + 1), 6):
                    # Building .class, .jar and running them
                    ant_bash_param1 = "-DN=" + str(N)
                    ant_bash_param2 = "-DI1=" + str(I1)
                    ant_bash_param3 = "-DI2=" + str(I2)

                    ant_bash_param_a = "-Da=" + str(sys.argv[index_arg_a+1])
                    ant_bash_param_p = "-Dp=" + str(index_arg_p)
                    ant_bash_param_t = "-Dt=" + str(index_arg_t)


                    j_bash_param1  = str(N)
                    j_bash_param2  = str(I1)
                    j_bash_param3  = str(I2)
                    j_bash_param_a = str(sys.argv[index_arg_a+1])
                    j_bash_param_p = str(index_arg_p)
                    j_bash_param_t = str(index_arg_t)

                    if index_conv > 0:
                        subprocess.call(
                            ["java", "-Xmx1g", "-Xms1g", "-jar", "build/jar/Algo_conventionnel.jar", 
                            j_bash_param1, 
                            j_bash_param2, j_bash_param3, j_bash_param_a, 
                            j_bash_param_p, j_bash_param_t, "-1", "-1"]
                        )
                        pass

                    if index_strassen > 0:
                        subprocess.call(
                            ["java", "-Xmx1g", "-Xms1g", "-jar", "build/jar/Algo_strassen.jar", 
                            j_bash_param1, 
                            j_bash_param2, j_bash_param3, j_bash_param_a, 
                            j_bash_param_p, j_bash_param_t, "-1", "-1"]
                        )
                        pass
                    if index_strassen_seuil > 0:
                        subprocess.call(
                            ["java", "-Xmx1g", "-Xms1g", "-jar", "build/jar/Algo_strassen_seuil.jar", 
                            j_bash_param1, 
                            j_bash_param2, j_bash_param3, j_bash_param_a, 
                            j_bash_param_p, j_bash_param_t, "-1", "-1"]
                        )
                        pass

                    pass
                pass
            pass

    else:

        ant_bash_param_a     = "-Da="     + str(sys.argv[index_arg_a + 1])
        ant_bash_param_p     = "-Dp="     + str(index_arg_p              )
        ant_bash_param_t     = "-Dt="     + str(index_arg_t              )
        ant_bash_param_path1 = "-Dpath1=" + str(sys.argv[index_path1]    )
        ant_bash_param_path2 = "-Dpath2=" + str(sys.argv[index_path2]    )

        j_bash_param1  = "-1"
        j_bash_param2  = "-1"
        j_bash_param3  = "-1"
        j_bash_param_a = str(sys.argv[index_arg_a + 1])
        j_bash_param_p = str(index_arg_p)
        j_bash_param_t = str(index_arg_t)
        j_bash_param_path1 = str(sys.argv[index_path1])
        j_bash_param_path2 = str(sys.argv[index_path2])

        if index_conv > 0:
            subprocess.call(
                ["java", "-Xmx1g", "-Xms1g", "-jar", "build/jar/Algo_conventionnel.jar", 
                j_bash_param1, 
                j_bash_param2, j_bash_param3, j_bash_param_a, 
                j_bash_param_p, j_bash_param_t, j_bash_param_path1, j_bash_param_path2]
            )
            pass
        if index_strassen > 0:
            subprocess.call(
                ["java", "-Xmx1g", "-Xms1g", "-jar", "build/jar/Algo_strassen.jar", 
                j_bash_param1, 
                j_bash_param2, j_bash_param3, j_bash_param_a,
                j_bash_param_p, j_bash_param_t, j_bash_param_path1, j_bash_param_path2]
            )
            pass
        if index_strassen_seuil > 0:
            subprocess.call(
                ["java", "-Xmx1g", "-Xms1g", "-jar", "build/jar/Algo_strassen_seuil.jar", 
                j_bash_param1, 
                j_bash_param2, j_bash_param3, j_bash_param_a,
                j_bash_param_p, j_bash_param_t, j_bash_param_path1, j_bash_param_path2]
            )
            pass
    # Cleaning the .jar and the .class files
    subprocess.call(["ant", "-q", "-S", "clean"])

else:
    print('Les arguments entre dans la commande sont invalide.')
    print('Veuillez entrez un des arguments suivant : ')
    print('\t-a\t[conv | strassen | strassenSeuil]')
    print('\t-e\t[path_vers_exemplaire_1 path_vers_exemplaire_2]')
    print('\t-p\taffiche la matrice résultat dans le format des exemplaires, sans texte superflu')
    print('\t-t\taffiche le temps d’exécution en ms, sans unité ni texte superflu')