Les arguments de la commande :
        -a      [conv | strassen | strassenSeuil]
        -e      [path_vers_exemplaire_1 path_vers_exemplaire_2]
        -p      affiche la matrice résultat dans le format des exemplaires, sans texte superflu
        -t      affiche le temps d’exécution en ms, sans unité ni texte superflu

    exemple :   ./tp.sh -a conv -t -p # va fair la mutliplier toutes les matrices entre elles
                ./tp.sh -a strasse -e src/ressources/matrix/ex_2.5 src/ressources/matrix/ex_2.4 # Va multiplier les matrices ex_1.1 et ex_1.2
